#include <stdio.h>
#include <cstdlib>
#include <iostream>

#include "reader.h"

#define MAXBUFLEN 20971520

FILE* __FP;
char* __BUFFER;
int __BUFFER_INDEX;


void prepare_file(char *filename) {
	__FP = fopen(filename, "r");
	__BUFFER_INDEX = MAXBUFLEN;
	__BUFFER = (char *)malloc(sizeof(char) * MAXBUFLEN);
}

void free_reader_ressources() {
	fclose(__FP);
	free(__BUFFER);
}

const void* read(int size) {
	if (__BUFFER_INDEX + size > MAXBUFLEN) {
		int diff = MAXBUFLEN - __BUFFER_INDEX;

		for (int i = __BUFFER_INDEX; i < MAXBUFLEN; i++) 
			__BUFFER[i - __BUFFER_INDEX] = __BUFFER[i];
		int chars_readed = fread(__BUFFER + diff, sizeof(char), MAXBUFLEN - diff, __FP);
		if (chars_readed == 0) return NULL;
		__BUFFER_INDEX = 0;

	}

	__BUFFER_INDEX += size;
	return (void *)(__BUFFER + (__BUFFER_INDEX - size));
}
