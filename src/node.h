#ifndef NODE_H
#define NODE_H

#define DEFAULT_SIZE 3

#include <cstdint>
#include <cstdlib>
#include <iostream>

#include <string>
#include <vector>

#include "reader.h"
#include "levenshtein.h"


/**
 *  @file node.h
 *  @brief Regroup all functions we can apply on a node
 */

/**
 *  @struct node
 *  @brief This struct describe a node of our radix trie, which is itself a particular radix trie
 *  @param freq_ : the frequence of the word where this node is the leaf of all nodes corresponding to this word
 *  @param children_size_ : the size of the array of children nodes
 *  @param children_capacity_ : the memory capacity of the array of children nodes
 *  @param part_size_ : the number of characters of the radix describe by part_ parameter
 *  @param terminaison_ : a boolean which marked the node as the end of a word (it's corresponding to freq_ != 0)
 *  @param part_ : a particular radix
 *  @param children_ : all children nodes of this node
 */
struct node {
    uint32_t freq_;
    uint8_t children_size_;
    uint8_t children_capacity_;
    uint8_t part_size_;
    bool terminaison_;
    char* part_;
    struct node** children_;
};

/**
 *  @struct word
 *  @brief This struct describe one the results of a search apply on the radix trie
 *  @param word_ : the word matching with the search
 *  @param frequency_ : the frequency of the matching word
 *  @param distance_ : the distance between the matching word and the word we're searching
 */
struct word {
	std::string* word_;
	uint32_t frequency_;
	uint8_t distance_;
};

/**
 *  @brief Copy chars from a const char* to another char*
 *  @param w_from : the word we want to copy
 *  @param w_from_wize : the size of the word we want to copy
 *  @return the copied word
 */
char* copy_word(const char* w_from, uint8_t w_from_size);

/**
 *  @brief Check the capacity of the children_ array of the node, and increase it when it's needed
 *  @param node : the node we want to check the capacity
 */
void check_capacity(struct node* node);

/**
 *  @brief Get the number of nodes in the trie
 *  @return the number of nodes
 */
int get_total_number_of_nodes();


/**
 *  @brief Get the number of chars in the trie
 *  @return the number of nodes
 */
unsigned long int get_total_number_of_chars();

/**
 *  @brief Create a node
 *  @param freq : the frequency of the associated word, or 0
 *  @param part : the radix string
 *  @param part_size : the size of the radix string
 *  @param terminaison : true if the radix is the end of a node's hierarchy representing a word
 *  @return a pointer to the created node
 */
struct node* create_node(uint32_t freq, const char* part, uint8_t part_size, bool terminaison);

/**
 *  @brief Clear the memory allocated for a node
 *  @param node : the node to free
 */
void clear_node(struct node* node);

/**
 *  @brief Add a node in the children array of another node
 *  @param root : the node we want to increase children
 *  @param child : the new child of root
 */
void append_node(struct node* root, struct node* child);

/**
 *  @brief Print a node recursively
 *  @param root : the root of the node hierarchy we want to print
 *  @param tab : num of the node generation, 0 for the root node 
 */
void pretty_print(struct node* root, int tab);

/**
 *  @brief Insert recursively a word in a root hierarchy. This function is called by insert method in trie.h
 *  @param root : the root of the node hierarchy where we try to add the word
 *  @param word : the word to insert
 *  @param word_size : the size of the word to insert
 *  @param word_freq : the frequency of the word
 *  @return true if the word was added to the node hierarchy, else otherwise
 */
bool insert(struct node* root, const char* word, uint8_t word_size, uint32_t word_freq);

/**
 *  @brief Serialize recursively a node
 *  @param node : the node to serialize
 *  @param file : the file where we serialize the node
 */
void serialize(struct node * node, FILE* file);

/**
 *  @brief Read recursively a serialized node hierarchy from a file. This function is called by serialize method in trie.h
 *  @return the read node
 */
struct node* read_node();

/**
 *  @brief Seach recursively a word in a node hierarchy. This function is called by search method in trie.h
 *  @param node : the root of the node hierarchy
 *  @param results : an std::vector where we store results
 *  @param dist : the max distance use in the damerau-levenshtein algorithm
 *  @param word : the word we are searching for. It's characters are consumed by the damerau-levenshtein algorithm
 *  @param word_size : the size of the word we are searching for
 *  @param i : parameter of damerau-levenshtein algorithm. Index of the last letter of the recursively constructed word with the node hierarchy.
 *  @param j : parameter of damerau-levenshtein algorithm. Index of the last letter of word.
 */
void search(struct node* node, std::vector<struct word*>& results, uint16_t dist, const char* word, uint16_t word_size, uint16_t i, uint16_t j);

/**
 *  @brief Allocate an array of nodes
 *  @param number_nodes : the number of nodes to store in the array
 */
void allocate_nodes_memory(int number_nodes);

/**
 *  @brief Get an available adress for a node
 *  @return the available adress
 */
struct node* get_free_node_address();

/**
 *  @brief Free the allocated array of nodes
 */
void free_nodes_memory();

/**
 *  @brief Allocate en array of chars
 *  @param number_chars : the size of the array
 */
void allocate_chars_memory(unsigned long int number_chars);

/**
 *  @brief Free the allocated array of chars
 */
void free_chars_memory();

std::ostream& operator<<(std::ostream& out, struct node *);

#endif /** NODE_H **/
