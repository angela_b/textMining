#ifndef LEVENSHTEIN_H
#define LEVENSHTEIN_H

#include <cstdint>
#include <cstdlib>
#include <list>

/**
 *  @file levenshtein.h
 *  @brief Damerau-Levenshtein algorithm, with the particularity of saving the constructed matrix for previous words
 */

/**
 *  @brief Allocate memory for matrix and params use in the Damerau-Levenshtein algorithm
 *  @param longest_word_size : the size of longest word we will use in the algorithm
 */
void init_levenshtein_parameters(uint16_t longest_word_size);

/**
 *  @brief Clear the memory use by matrix and params
 */
void clear_levenshtein_parameters();

/**
 *  @brief Get the vertical constructed word use in the algorithm. This function is made for search function of node.h
 */
char* get_levenshtein_current_word();

/**
 *  @brief Get the distance between two words using the damerau_levenshtein algorithm
 *  @param max_dist : the maximal distance between the two words
 *  @param w1_part : the radix of the vertical word
 *  @param w2_part : the radix of the horizontal word
 *  @param l_w1 : the length of w1_part
 *  @param l_w2 : the length of w2_part
 *  @param pi : the index of w1_part in the vertical word
 *  @param pj : the index of w2_part in the horizontal word
 */
int16_t damerau_levenshtein(uint16_t max_dist, const char* w1_part, const char* w2_part,
                            uint16_t l_w1, uint16_t l_w2, uint16_t pi, uint16_t pj);
#endif /** LEVENSHTEIN_H **/