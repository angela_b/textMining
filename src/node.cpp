#include "node.h"

#define CAPACITY_INCR 8

int __TOTAL_NODES = 0;
int __WRITTEN_NODES = -1;
struct node* __NODES_ARRAY;

unsigned long int __NUM_CHARS = 0;
unsigned long int __WRITTEN_CHARS = -1;
char * __CHARS_ARRAY;

void allocate_nodes_memory(int number_nodes) {
    __TOTAL_NODES = number_nodes;
    __NODES_ARRAY = (struct node *)malloc(sizeof(struct node) * __TOTAL_NODES);
    __WRITTEN_NODES = 0;
}

void allocate_chars_memory(unsigned long int number_chars) {
    __NUM_CHARS = number_chars;
    __CHARS_ARRAY = (char *)malloc(sizeof(char) * __NUM_CHARS);
    __WRITTEN_CHARS = 0;
}

void free_nodes_memory() {
    free(__NODES_ARRAY);
}

struct node* get_free_node_address() {
    if (__WRITTEN_NODES == -1) return (struct node *)malloc(sizeof(struct node));
    __WRITTEN_NODES += 1;
    return __NODES_ARRAY + __WRITTEN_NODES - 1;
}

char * get_free_char_address(long unsigned int len) {
    if (__WRITTEN_CHARS == -1) return (char *)malloc(sizeof(char) * len);
    __WRITTEN_CHARS += len;
    return __CHARS_ARRAY + __WRITTEN_CHARS - len;
}

void free_chars_memory() {
    free(__CHARS_ARRAY);
}

char* copy_word(const char* w_from, uint8_t w_from_size) {
    char* w_to = get_free_char_address(w_from_size);
    char* tmp = w_to;
    int i = 0;
    while (i < w_from_size) {
        *w_to = *(w_from + i);
        w_to++;
        i++;
    }
    return tmp;
}

void check_capacity(struct node* node) {
    if (node->children_capacity_ == node->children_size_) {
        node->children_ = (struct node **) realloc(node->children_, (node->children_capacity_ + CAPACITY_INCR) * sizeof(struct node **));
        node->children_capacity_ += CAPACITY_INCR;
    }
}


int get_total_number_of_nodes() {
    return __TOTAL_NODES;
}

unsigned long int get_total_number_of_chars() {
    return __NUM_CHARS;
}

struct node* create_node(uint32_t freq, const char* part, uint8_t part_size, bool terminaison) {
    char* allocated_part = copy_word(part, part_size);

    __NUM_CHARS += (long unsigned int)part_size;

    struct node* node = get_free_node_address();
    node->freq_ = freq;
    node->terminaison_ = terminaison;
    node->children_capacity_ = 0;
    node->children_size_ = 0;
    node->children_ = nullptr;
    node->part_size_ = part_size;
    node->part_ = allocated_part;

    __TOTAL_NODES += 1;

    return node;
}

void clear_node(struct node* node) {

    for (int i = 0; i < node->children_size_; i++) {
        clear_node(node->children_[i]);
    }

    free(node->children_);

    if (__WRITTEN_NODES != -1) return;

    free(node->part_);
    free(node);
}

void append_node(struct node* root, node* child) {
    check_capacity(root);
    root->children_[root->children_size_] = child;
    root->children_size_ += 1;
}

void pretty_print(struct node* root, int tab) {
    for (int i = 0; i < tab; i++) { std::cerr << "|\t"; }
    std::cerr << root->part_ << ' ' << (root->terminaison_ ? 'T' : 'F') << std::endl;
    for (int i = 0; i < root->children_size_; i++)
        pretty_print(root->children_[i], tab + 1);
}

bool insert(struct node* root, const char* word, uint8_t word_size, uint32_t word_freq) {

    int i = 0, j = 0;
    for (; i < root->part_size_ && j < word_size && root->part_[i] == word[j]; i++, j++) { }

    if (i == 0 && j == 0) return false;
    if ((i == root->part_size_) && (j == word_size)) {
        root->terminaison_ = true;
        root->freq_ += word_freq;
        return true;
    }

    if (i == root->part_size_) {
        bool child_match = false;
        for (int k = 0; k < root->children_size_ && !child_match; k++) {
            child_match = insert(root->children_[k], word + j, word_size - (uint8_t)j, word_freq);
        }
        if (!child_match) {
            struct node* child = create_node(word_freq, word + j, word_size - (uint8_t)j, true);
            append_node(root, child);
            return true;
        }
    } else {
        struct node* child_a = create_node(root->freq_, root->part_ + i, root->part_size_ - (uint8_t)i, root->terminaison_);
        __NUM_CHARS -= (long unsigned int)root->part_size_ - (long unsigned int)i;
        root->part_size_ = (uint8_t )i;
        root->part_ = (char *) realloc(root->part_, (size_t)i);
        root->terminaison_ = (j == word_size);
        root->freq_ = (j == word_size) ? word_freq : 0;
        child_a->children_ = root->children_;
        child_a->children_size_ = root->children_size_;
        child_a->children_capacity_ = root->children_capacity_;
        root->children_capacity_ = 0;
        root->children_size_ = 0;
        root->children_ = nullptr;
        append_node(root, child_a);

        if (j < word_size) {
            struct node* child_b = create_node(word_freq, word + j, word_size - (uint8_t )j, true);
            append_node(root, child_b);
        }
        return true;
    }
    return true;
}

void serialize(struct node * node, FILE* file) {
    fwrite(&node->freq_, 1, sizeof(uint32_t), file);
    fwrite(&node->part_size_, 1, sizeof(uint8_t), file);
    fwrite(&node->children_size_, 1, sizeof(uint8_t), file);
    fwrite(&node->terminaison_, 1, sizeof(bool), file);
    fwrite(node->part_, node->part_size_, sizeof(char), file);
    for (int i = 0; i < node->children_size_; i++)
        serialize(node->children_[i], file);
}

struct node* read_node() {
    node* node = get_free_node_address();//(struct node *) malloc(sizeof(struct node));
    
    node->freq_ = *(uint32_t *)read(sizeof(uint32_t));
    node->part_size_ = *(uint8_t *)read(sizeof(uint8_t));
    node->children_size_ = *(uint8_t *)read(sizeof(uint8_t));
    node->terminaison_ = *(bool *)read(sizeof(bool));

    node->children_capacity_ = node->children_size_;
    node->part_ = get_free_char_address((unsigned long int)node->part_size_);// (char *) malloc(node->part_size_ * sizeof(char));

    for (int i = 0; i < node->part_size_; i++)
        *(node->part_ + i) = *(char *)read(sizeof(char));
        
    node->children_ = (struct node * *) malloc(sizeof(struct node *) * node->children_size_);
    for (int i = 0; i < node->children_size_; i++) {
        struct node * child = read_node();
        node->children_[i] = child;
    }
    return node;
}


void search(struct node* node, std::vector<struct word*>& results, uint16_t dist, const char* word, uint16_t word_size, uint16_t i, uint16_t j) {
    int16_t res = damerau_levenshtein(dist, node->part_, word, node->part_size_, word_size, i, j);

    if (res == -1) { return; }
    if (res != -2 && node->terminaison_) {
        struct word *trie_word = (struct word *) malloc(sizeof(struct word));
	char * word = get_levenshtein_current_word();
        trie_word->word_ = new std::string(get_levenshtein_current_word());
	trie_word->distance_ = res;
        trie_word->frequency_ = node->freq_;
        results.push_back(trie_word);
    }
    for (int k = 0; k < node->children_size_; k++) {
        search(node->children_[k], results, dist, "", 0, i + node->part_size_, j + word_size);
    }
}

std::ostream& operator<<(std::ostream& out, struct node* node) {
    out << "(" << node->part_ << ", " << node->freq_ << ", " << (node->terminaison_ ? "T" : "F") << ")";
    return out;
}
