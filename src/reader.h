#ifndef READER_H
#define READER_H

/**
 *  @file reader.h
 *  @brief Improvement of the time spent when reading a trie from disk
 */


/**
 *  @brief Open a file and init a buffer
 *  @param filename : the name of the file containing the serialized trie
 */
void prepare_file(char *filename);

/**
 *  @brief Free all used ressources
 */
void free_reader_ressources();

/**
 *  @brief Read a binary type from the buffer
 *  @param size : the size in bytes of the type to read
 *  @return A pointer to the readed type
 */
const void* read(int size);

#endif /** READER_H **/