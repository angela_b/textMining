#include "trie.h"

void read_trie(char* filename, std::vector<struct node *>& trie) {

    prepare_file(filename);
    int size = *(int *)read(sizeof(int));
    int number_nodes = *(int *)read(sizeof(int));
    int number_chars = *(unsigned long int *)read(sizeof(unsigned long int *));
    allocate_nodes_memory(number_nodes);
    allocate_chars_memory(number_chars);
    trie.reserve(size);

    for (int i = 0; i < size; i++) {
        trie.push_back(read_node());
    }

    free_reader_ressources();
}

void serialize(std::vector<struct node *>& trie, FILE* file) {
    int size = (int)trie.size();
    int number_nodes = get_total_number_of_nodes();
    unsigned long int number_chars = get_total_number_of_chars();
    fwrite(&size, 1, sizeof(int), file);
    fwrite(&number_nodes, 1, sizeof(int), file);
    fwrite(&number_chars, 1, sizeof(unsigned long int), file);

    for (auto node : trie) serialize(node, file);
}

void insert(std::vector<struct node *>& trie, const char* word, uint8_t word_size, uint32_t word_freq) {
    bool child_match = false;
    for (int i = 0; i < trie.size() && !child_match; i++)
        child_match = insert(trie[i], word, word_size, word_freq);
    if (!child_match) {
        struct node* child = create_node(word_freq, word, word_size, true);
        trie.push_back(child);
    }
}

void pretty_print(std::vector<struct node *>& trie) {
    for (auto node : trie) {
        pretty_print(node, 0);
    }
}

void clear(std::vector<struct node *>& trie) {
    for (auto node : trie)
        clear_node(node);
}

void search(std::vector<struct word *>& results, std::vector<struct node *>& trie, uint8_t dist, const char* word, uint16_t word_size) {
    for (auto node : trie) {
        search(node, results, dist, word, word_size, 1, 1);
    }
}