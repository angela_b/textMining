#include "levenshtein.h"

#include <iostream>

uint16_t __L_MAT_SIZE;
uint16_t* __L_MAT;

char* __W1_BUF;
char* __W2_BUF;

uint16_t min(const uint16_t* arr, uint16_t size) {
    uint16_t min = UINT16_MAX;
    for (int i = 0; i < size; i++) {
        min = min > *(arr + i) ? *(arr + i) : min;
    }
    return min;
}

uint16_t min(std::initializer_list<uint16_t> args) {
    uint16_t min = UINT16_MAX;
    for (auto i : args) {
        min = min > i ? i : min;
    }
    return min;
}

uint16_t min(uint16_t n1, uint16_t n2, uint16_t n3) { return min({n1, n2, n3}); }
uint16_t min(uint16_t n1, uint16_t n2) { return min({n1, n2}); }

void init_levenshtein_parameters(uint16_t longest_word_size) {

	__L_MAT_SIZE = longest_word_size + 1;
	__L_MAT = (uint16_t *) malloc(sizeof(uint16_t) * __L_MAT_SIZE * __L_MAT_SIZE);
    for (uint16_t i = 0; i < __L_MAT_SIZE; i++) { 
    	__L_MAT[__L_MAT_SIZE * 0 + i] = i; 
    	__L_MAT[__L_MAT_SIZE * i + 0] = i; 
    }

    __W1_BUF = (char *) malloc(sizeof(char) * (__L_MAT_SIZE + 1));
    __W2_BUF = (char *) malloc(sizeof(char) * (__L_MAT_SIZE + 1));

    for (int i = 0; i < __L_MAT_SIZE + 1; i++) {
      __W1_BUF[i] = '\0';
      __W2_BUF[i] = '\0';
    }
}

void clear_levenshtein_parameters() {
	free(__L_MAT);
	free(__W1_BUF);
	free(__W2_BUF);
}


void splited_damerau_levenshtein(uint16_t i, uint16_t j) {
    uint16_t substitution_cost = 1;

    if (__W1_BUF[i] == __W2_BUF[j]) substitution_cost = 0;

        __L_MAT[__L_MAT_SIZE * i + j] = min(__L_MAT[__L_MAT_SIZE * (i - 1) + j] + 1,
        									__L_MAT[__L_MAT_SIZE * i + (j - 1)] + 1,
        									__L_MAT[__L_MAT_SIZE * (i - 1) + (j - 1)] + substitution_cost);

        if (i > 1 && j > 1 && __W1_BUF[i] == __W2_BUF[j - 1] && __W1_BUF[(i - 1)] == __W2_BUF[j]) {
            __L_MAT[__L_MAT_SIZE * i + j] = min(__L_MAT[__L_MAT_SIZE * i + j], 
                                                __L_MAT[__L_MAT_SIZE * (i - 2) + (j - 2)] + substitution_cost);
        }

}

char* get_levenshtein_current_word() {
	return __W1_BUF + 1;
}

int16_t damerau_levenshtein(uint16_t max_dist, const char* w1_part, const char* w2_part,
                         	uint16_t l_w1, uint16_t l_w2, uint16_t pi, uint16_t pj) {

    auto l_tot_w1 = l_w1 + pi - 1;
    auto l_tot_w2 = l_w2 + pj - 1;

    if (l_tot_w1 > l_tot_w2 && l_tot_w1 - l_tot_w2 > max_dist) return -1;

    for (auto i = pi; i < l_w1 + pi; i++) __W1_BUF[i] = w1_part[i - pi];
    for (auto j = pj; j < l_w2 + pj; j++) __W2_BUF[j] = w2_part[j - pj];


    for (auto i = pi; i < pi + l_w1; i++) {
        for (auto j = 1; j < pj; j++) {
            splited_damerau_levenshtein(i, j);            
        }
    }

    for (auto i = 1; i < pi; i++) {
        for (auto j = pj; j < pj + l_w2; j++) {
            splited_damerau_levenshtein(i, j);
        }
        if (i < pj + l_w2 && min(__L_MAT + __L_MAT_SIZE * i, pj + l_w2) > max_dist) return -1;
    }
            
    for (auto i = pi; i < pi + l_w1; i++) {
        for (auto j = pj; j < pj + l_w2; j++) {
            splited_damerau_levenshtein(i, j);
        }
        if (i < pj + l_w2 && min(__L_MAT + __L_MAT_SIZE * i, pj + l_w2) > max_dist) return -1;
    }

    auto res = __L_MAT[__L_MAT_SIZE * (l_w1 + pi - 1) + (l_w2 + pj - 1)];
    if (res <= max_dist) { 
        __W1_BUF[l_w1 + pi] = '\0'; 
    }
    return res > max_dist ? -2 : res;
}
