#ifndef TRIE_H
#define TRIE_H

#include <cstdint>
#include <cstdlib>
#include <iostream>
#include <vector>

#include "node.h"
#include "reader.h"
#include "levenshtein.h"


/**
 *  @file trie.h
 *  @brief Regroup all functions we can apply on our trie
 */


/**
 *  @brief Read a trie from a file
 *  @param filename : the file where is serialized the trie
 *  @param trie : the std::vector when we add all root nodes
 */
void read_trie(char* filename, std::vector<struct node *>& trie);

/**
 *  @brief Serialize a trie to a file
 *  @param trie : the std::vector of root nodes we want to serialize
 *  @param file : the file where we want to serialize the trie
 */
void serialize(std::vector<struct node *>& trie, FILE* file);

/**
 *  @brief Insert a word in the trie
 *  @param trie : the std::vector of root nodes
 *  @param word : the word we want to has in the trie
 *  @param word_size : the size of the word, without the '\0' character
 *  @param word_freq : the frequence of the word
 */
void insert(std::vector<struct node *>& trie, const char* word, uint8_t word_size, uint32_t word_freq);


/**
 *  @brief Print a trie
 *  @param trie : the std::vector of root nodes to pretty print
 */
void pretty_print(std::vector<struct node *>& trie);

/**
 *  @brief Clear the memory used by the trie
 *  @param trie : the std::vector of root nodes to clear
 */
void clear(std::vector<struct node *>& trie);

/**
 *  @brief Search a word in the trie
 *  @param results : the std::vector where we store the results
 *  @param trie : the std::vector of root nodes where we apply the search
 *  @param dist : the max distance use in the damerau-levenshtein algorithm
 *  @param word : the word we are searching for
 *  @param word_size : the size of the word we are searching for
 */
void search(std::vector<struct word *>& results, std::vector<struct node *>& trie, uint8_t dist, const char* word, uint16_t word_size);

#endif /** TRIE_H **/