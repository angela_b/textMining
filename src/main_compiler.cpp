#include <iostream>
#include <sstream>
#include <fstream>
#include <vector>
#include <string>
#include <chrono>

#include "node.h"
#include "trie.h"

int main(int argc, char** argv) {

    if (argc != 3) {
        std::cerr << "Usage: " << argv[0] << " /path/to/word/freq.txt /path/to/output/dict.bin" << std::endl;
        exit(134);
    }

    std::vector<node *> trie;

    std::string word_freq_path(argv[1]);
    char* dict_path = argv[2];

    std::ifstream source(word_freq_path);
    FILE* dest = fopen(dict_path, "w");

    std::string word;
    uint32_t frequency;

    std::cerr << "[INFO]" << " Computing trie from file: " << word_freq_path << std::endl;
    while (source >> word >> frequency) {
        insert(trie, word.c_str(), word.size(), frequency);
    }

    std::cerr << "[INFO]" << " Writting trie in file: " << dict_path << std::endl;
    serialize(trie, dest);

    clear(trie);

    fclose(dest);

    return 0;
}