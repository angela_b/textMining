#include <iostream>
#include <sstream>
#include <fstream>
#include <vector>
#include <string>
#include <chrono>
#include <regex>


#include "node.h"
#include "trie.h"

bool word_strict_less(struct word* w1, struct word* w2) {
    // Priorité sur la distance
    if (w1->distance_ > w2->distance_)
        return false;
    else if (w1->distance_ == w2->distance_) {
        // Sinon frequence
        if (w1->frequency_ > w2->frequency_)
            return true;
            // Sinon l'ordre lexicographique **/
        else if (w1->frequency_ == w2->frequency_)
	  return w1->word_->compare(*(w2->word_));
        else return false;
    }
    else
        return true;
}


int main(int argc, char** argv) {

    if (argc != 2) {
        std::cerr << "Usage: " << argv[0] << "/path/to/dict.bin" << std::endl;
        exit(134);
    }


    char* dict_path(argv[1]);

    std::vector<node *> trie;

    std::cerr << "[INFO]\t" << " Reading trie from file: " << dict_path << std::endl;
    read_trie(dict_path, trie);


    std::cerr << "[INFO]\t" << " Trie successfully loaded." << std::endl;
    
    std::regex e ("\\S+");
    std::string line;
    while (std::getline(std::cin, line)) {
        std::vector<std::string> words;
        std::regex_iterator<std::string::iterator> rit(line.begin(), line.end(), e);
        std::regex_iterator<std::string::iterator> rend;
        while (rit != rend) {
            words.push_back(rit->str());
            rit++;
        }

        if (words.size() == 1 && words[0] == std::string("quit")) break;
        if (words.size() == 1 && words[0] == std::string("trie")) { pretty_print(trie); };
        if (words.size() != 3 || words[0] != std::string("approx")) continue;
        else {
            uint16_t n = (uint16_t)std::stoi(words[1]);
            std::vector<struct word *> results;
            init_levenshtein_parameters(n + (uint16_t)words[2].size());

            auto start = std::chrono::system_clock::now();
            search(results, trie, n, words[2].c_str(), words[2].size());
            auto end = std::chrono::system_clock::now();
            std::chrono::duration<double> diff = end-start;

            std::cerr << "[INFO]\t" << " " << results.size() << " matching word(s)." << std::endl;
            std::cerr << "[INFO]\t" << " time spent: " << diff.count() << 's' << std::endl;

            std::sort(results.begin(), results.end(), word_strict_less);
            std::cout << '[';
            bool begin = true;
            for (auto result : results) {
                std::cout << (begin ? "" : ",")
                          << "{\"word\":\"" << *(result->word_) << "\",\"freq\":" << result->frequency_ << ",\"distance\":" << (int)result->distance_ << "}";
                begin = false;
                delete result->word_;
                free(result);
            }
            std::cout << ']' << std::endl; 

            clear_levenshtein_parameters();
        }
    }

    clear(trie);
    free_nodes_memory();
    free_chars_memory();
    
    return 0;
}

