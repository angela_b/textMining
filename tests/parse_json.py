import json
import subprocess
from pprint import pprint
import time

bin_compiler_ref = "./ref/osx/TextMiningCompiler"
bin_app_ref = "./TextMiningAppRef"
bin_compiler_test = "./TextMiningCompiler"
bin_app_test = "./TextMiningApp"

dict_ref = "./dict_ref.bin"
dict_test = "./dict_test.bin"

def get_cmd(word, distance):
    return "approx " + str(distance) + " " + word

def read_from_json(json_string):
    dic = json.loads(json_string)
    return dic

def exec_approx(word, distance, ref = True):
    cmd = "echo \"" + get_cmd(word, distance) + "\" | " + (bin_app_ref + " " + dict_ref if ref else bin_app_test + " " + dict_test)
    #print("Commande ",cmd)
    cmd_exec = subprocess.Popen(cmd, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True )
    #   print("Commande exec", cmd_exec)
    json_res, _ = cmd_exec.communicate()
    #print("Json res ",json_res)
    json_res = json_res.decode('utf8')
    return read_from_json(json_res)

def get_missing(word, distance):
    res_ref = exec_approx(word, distance)
    res_test = exec_approx(word, distance, ref = False)

    error_found = False
    if (len(res_ref) != len(res_test)):
        error_found = True
        print("[MISSING] dictionnary not the same size, our ",len(res_test), " his ", len(res_ref))
    return error_found

def get_missing_slow(word, distance):
    res_ref = exec_approx(word, distance)
    res_test = exec_approx(word, distance, ref = False)

    error_found = False
    if (len(res_ref) != len(res_test)):
        error_found = True
        print("[MISSING] dictionnary not the same size, our ",len(res_test), " his ", len(res_ref))

    for dic1 in res_ref:
        if not dic1 in res_test:
            print("Error found ")
            return True

    return error_found


f = open("words.txt", 'r')
lignes = f.readlines()
f.close()
import re
import random
import string

cpt = 0
def test_efficiency(cpt):
    compteur = 0
    for lines in lignes :
        compteur = compteur + 1
        splitted = lines.split(" ")

        regex = re.compile(r'[\n\r\t]')
        phrase = regex.sub(" ", splitted[0])
        second_p = phrase.split(" ")
        phrase = second_p[0]
        dist = random.randint(0, len(phrase))
        print(phrase+" distance : ",dist)
        try:
            err_code = get_missing(phrase, dist)
            if (err_code):
                print("Error ")
            else:
                print("Ok")
        except Exception :
            print("oups exception")
        if (compteur > cpt):
            return
def test_speed(max_cpt):
    print("Test vitesse ")

    time_string_d1 = ""
    time_string_d0 = ""
    time_string_d2 = ""
    cpt = 0
    for ligne in lignes:
        cpt = cpt + 1
        splitted = ligne.split(" ")
        regex = re.compile(r'[\n\r\t]')
        phrase = regex.sub(" ", splitted[0])
        second_p = phrase.split(" ")
        phrase = second_p[0]
        dist = random.randint(0, len(phrase))
        #err_code = get_missing(phrase, dist)

        time_string_d1 = time_string_d1 + "echo \"" +  get_cmd(phrase, 1)+ "\" | " + bin_app_test + " " + dict_test+ "\n"
        time_string_d0 = time_string_d0 +"echo \"" +  get_cmd(phrase, 0)+ "\" | " + bin_app_test + " " + dict_test+ "\n"
        time_string_d2 = time_string_d2 + "echo \"" +  get_cmd(phrase, 2)+ "\" | " + bin_app_test + " " + dict_test+ "\n"
        if (cpt % 30 == 0):
            tps1 = time.clock()
            cmd_exec = subprocess.Popen(time_string_d2, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE,
                                        shell=True)
            json_res, _ = cmd_exec.communicate()
            tps2 = time.clock() - tps1

            print("Temps d'execution distance 2 ", tps2)# 		#print(time_string_d2)
            time_string_d2 = ""
        if (cpt % 300  == 0):
            tps1 = time.clock()
            cmd_exec = subprocess.Popen(time_string_d1, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE,
                                        shell=True)
            json_res, _ = cmd_exec.communicate()

            tps2 = time.clock() - tps1
            print("Temps d'execution distance 1 ", tps2)
            time_string_d1 = ""
        #print(time_string_d1)
        if (cpt % 3000 == 0):
            tps1 = time.clock()
            cmd_exec = subprocess.Popen(time_string_d0, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE,
                                        shell=True)
            json_res, _ = cmd_exec.communicate()
            tps2 = time.clock() - tps1
            print("Temps d'execution distance 0 ======= ", tps2)
            time_string_d0 = ""
        #print(time_string_d0)
        if (cpt > max_cpt):
            break

test_efficiency(100)
test_speed(100)